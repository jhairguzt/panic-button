from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from hashing.hashing import generate_salt, generate_hash

Base = declarative_base()


class Guard(Base):
    __tablename__ = "guard"

    id    = Column(Integer, primary_key=True)
    token = Column(String(300))
    name  = Column(String(200))
    email = Column(String(50))
    salt  = Column(String(50))
    phash = Column(String(50))
    able  = Column(Boolean)


    def is_able(self):
        return self.able == True


    def set_password(self, password):
        # generate random salt
        self.salt = generate_salt()
        # generate hash with password and salt
        self.phash = generate_hash(self.salt, password)
    
    
    def confirm_password(self, password):
        # obtain the hash by the password
        return self.phash == generate_hash(self.salt, password)


    def __repr__(self):
        return "<Guard(id='{}', token='{}', name='{}')>".format(self.id, self.token, self.name)


class Button(Base):
    __tablename__ = "button"

    id = Column(Integer, primary_key=True)
    code = Column(String(50), nullable=False)

    def __repr__(self):
        return "<Button(id='{}', code='{}')>".format(self.id, self.code)


class Emergency(Base):
    __tablename__ = "emergency"

    id = Column(Integer, primary_key=True)
    time = Column(String(8))
    date = Column(String(10))

    button_id = Column(Integer, ForeignKey('button.id'))
    button = relationship(Button)

    def __repr__(self):
        return "<Emergency(id='{}', time='{}', date='{}', button_id='{}')>".format(self.id, self.time, self.date, self.button_id)


class Guard_Temp(Base):
    __tablename__ = "guard_temp"

    id = Column(Integer, primary_key=True)

    guard_id = Column(Integer, ForeignKey('guard.id'))
    guard = relationship(Guard)

    def __repr__(self):
        return "<Guard_Temp(id='{}', guard_id='{}')>".format(self.id, self.guard_id)
