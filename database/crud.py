from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .config import DATABASE_URI
from .models import Guard, Button, Emergency

engine = create_engine(DATABASE_URI)
DBSession = sessionmaker(bind=engine)


##############################################################################################
############################## FUNCTIONS TO MANAGE API REQUESTS ##############################
##############################################################################################


def get_guardians_tokens_db():
    session = DBSession()
    guards  = session.query(Guard).all()
    tokens  = [guard.token for guard in guards if guard.is_able()]
    return tokens


def keep_guardian_token_db(json):
    token, email = json['token'], json['email']
    session      = DBSession()
    guard        = session.query(Guard).filter_by(email=email).one()
    guard.token  = token
    guard.able   = True

    session.commit()


# VALIDATION JUST WITH BUTTON CODE
def register_emergency_db(data):
    code, time, date = data['code'], data['time'], data['date']

    session   = DBSession()
    button    = session.query(Button).filter_by(code=code).one()
    emergency = Emergency(time=time, date=date, button=button)
    
    session.add(emergency)
    session.commit()


def validate_login(json):
    email, password = json['email'], json['password']
    
    session = DBSession()
    guard = session.Query(Guard).filter_by(email=email).one()

    return guard.confirm_password(password)


def add_guard(data):
    name, email, password = data["name"], data["email"], data["password"]
    guard = Guard(name=name, email=email, able=False)
    guard.set_password(password)

    session = DBSession()
    session.add(guard)
    session.commit()


def add_button(code):
    button = Button(code=code)

    session = DBSession()
    session.add(button)
    session.commit()
