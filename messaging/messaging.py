from os import path
from firebase_admin import credentials, initialize_app, messaging

cred = credentials.Certificate(path.join(path.dirname(__file__), 'panico-3d847-firebase-adminsdk-x9rhh-b5fb6b2c0f.json'))
app = initialize_app(cred)


def send_android_message(token, data):
    # ALL DATA CONTENT MUST BE STRING VALUES
    for key in data.keys():
        data[key] = str(data[key])

    message = messaging.Message(
        android = messaging.AndroidConfig(priority='high'),
        token = token,
        data = data
    )
    
    response = messaging.send(message)
