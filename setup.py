from database.crud import engine
from database.models import Base

from hashing.hashing import generate_salt
from database.crud import add_guard, add_button


# from crud import engine
# from models import Base

import sys


def create_database():
    Base.metadata.create_all(engine)


def drop_database():
    Base.metadata.drop_all(engine)


def recreate_database():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


# implemented user accounts generator
def generate_guard_accounts():
    data = {}
    
    for enter in ["name", "email", "password"]:
        print(enter,": ", sep="", end="")
        data[enter] = input()
    
    add_guard(data)
    print("Keep the follow data: ")
    print("[CSV FORMAT]: {},{},{}".format(data["name"], data["email"], data["password"]))
    print("Guard created")


# implemented button codes generator
def generate_button_codes():
    code = generate_salt(50)
    add_button(code)
    
    print("Keep the follow data: ")
    print("code: {}".format(code), end="\n\n")
    print("Button created")


def main():
    assert len(sys.argv) > 1, "Not enough arguments"

    # to generate button codes or user accounts
    if "-g" in sys.argv:
        try:
            command = sys.argv[sys.argv.index('-g')+1]
            if command == 'button':
                generate_button_codes()
            elif command == 'guard':
                generate_guard_accounts()
            else:
                print("Bad argument for -g (button / guard)")
        
        except IndexError:
            print("Argument for -g missing")
    
    # to create for the first time
    if "-c" in sys.argv:
        create_database()
        print("Database created successfully")

    # to recreate the database, WARNING    
    if "-r" in sys.argv:
        print("[WARNING] Are you sure to drop actual database and recreate it? (Y/N): ", end="")
        decision = input()
        if decision.upper() in ["YES", "Y"]:
            recreate_database()
            print("Database recreated successfully")
        else:
            print("No changes in database")

    if "-d" in sys.argv:
        print("[WARNING] Are you sure to drop actual database? (Y/N): ", end="")
        decision = input()
        if decision.upper() in ["YES", "Y"]:
            drop_database()
            print("Database recreated successfully")
        else:
            print("No changes in database")


if __name__ == '__main__':
    main()
