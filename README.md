# PANIC BUTTON
Backend para el proyecto del botón de pánico para el Open Pucp.

## REQUEST PARA EL SERVICIO WEB
Existen 3 únicos servicios de la aplicación:
- enviar alertas desde los botones a los guardias
- guardar los tokens necesarios para la comunicación de notificaciones de pánico
- permitir el logueo de los guardias a la aplicación

Cada uno de estos servicios necesita de un request POST con un  `body` detallado líneas abajo.

### 1. ENVIAR ALERTAS DESDE BOTONES
`api/alert` activa la función `alert()`. Primero se valida que el que envía sea un botón con un código válido. Luego de ello, se procede a enviar a cada guardia registrado y activo una notificación de pánico.<br>

__REQUEST:__ POST <br>

__BODY:__ El código debe ser uno válido entregado por el desarrollador.<br>
```js
{
    "code" : "104ejemplo104CoDe"
}
```
__Observación:__ Asegurarse de que el código posea una longitud de 20 a 50 caracteres.<br><br>

__HEADER:__
```js
{
    "Content-Type" : "application/json"
}
```

### 2. GUARDAR TOKEN DE GUARDIA
`api/guards` activa la función `keep_guard_token()`. Los guardias se encuentran guardados ya en la base de datos con los nombres de cada uno, y además a cada uno se le ha asignado un email y contraseña. Para guardar el token y poder enviarle notificaciones, solo es necesario su email y su token generado por su smartphone. <br>

__REQUEST:__ POST <br>

__BODY:__
- token: generado por el mismo smartphone.
- email: correo generado por desarrollador y usado por usuario para loguearse por primera vez.
```js
{
    "token" : "104ejemplo104ToKen",
    "email" : "usuario@open.pucp.pe"
}
```
__Observación:__ 
- token: 100 a 300 caracteres
- email: 15 a 50 caracteres, y debe llevar "@open.pucp.pe".
<br><br>


__HEADER:__
```js
{
    "Content-Type" : "application/json"
}
```

### 3. PERMITIR LOGUEO DE GUARDIA
`api/login` activa la función `login_guard()`.Este permite al guardia, de acuerdo al email y contraseña otorgados, loguearse en la aplicación y generar un token para que reciba las notificaciones de pánico.<br><br>

__REQUEST:__ POST <br>

__BODY:__
- email: otorgado por el desarrollador.
- password: otorgado por el desarrollador.
```js
{
    "email" : "usuario@open.pucp.pe",
    "password" : "104ejemplo104PaSwOrD"
}
```

__Observación:__ 
- email: 15 a 50 caracteres y debe llevar "@open.pucp.pe".
- password: 5 a 50 caracteres.
<br><br>

__HEADER:__
```js
{
    "Content-Type" : "application/json"
}
```

<hr>

## ERRORES DE SERVIDOR
El servicio actualmente se encuentra alojado en Heroku. Los principales errores que se han encontrado al testear los servicios son los siguientes.

### Error 400
Error común sobre una mal formulación del request POST. Tener en cuenta la cabecera `"Content-Type" : "application/json"`.

### Error 50x
Error por parte del servidor. Normalmente el servidor, después de 30 minutos de inactividad, "se duerme", por lo que tardará unos segundos extras en responder, y puede que tanto el botón como la app no estén preparadas para ese delay y reciban un error 50x.

También esto se debe a alguna incorrecta validación de los datos recibidos dentro de las funciones implementadas. De ser el caso, comunicarse con el desarrollador.


## RESPUESTAS DE REQUEST
Las respuestas siempre serán devueltas en tipo `json`. De hecho, poseen una estructura común para todos los servicios.

```js
{
    "status" : "_",
    "code" : _,
    "message" : "_"
}
```

__OBSERVACIÓN:__ A pesar de recibir un código 200 por realizar correctamente el request (statuscode = 200), dentro de los servicios existen validaciones que generan `json`s con esta estructura pero con códigos `400`, `401`. 

### 1. status
De tipo `string`. Puede tener alguno de los siguientes valores: `"ok"` o `"error"`. `"ok"` es acompañado siempre de un código 200. Por otro lado, `"error"` puede estar acompañado por distintos códigos 40x.

### 2. code
De tipo `int`. Presenta los siguientes valores:

| Código | Descripción |
|  ---   |     ---     |
|  200   | valores correctos |
|  400   | error en validación de valores de json |
|  401   | error con permisos (error en validación a nivel de autenticación) |

### 3. message
Una breve descripción sobre el resultado del servicio en caso  de código 200. Detalles del error en caso de código 40x.